#! /usr/bin/env python3
#
# jgensub.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

# disable writing bytecode (uses encoding hack)
import sys as _sys
_sys.dont_write_bytecode = True

import subprocess
import sys
from sys import stderr, stdout
import traceback

from jobgen.constants import VERSION

def print_usage():
	print("""\
usage: jgensub [<options>] -p <profname> -j <jobfile> [-- <jobgen-args>]
       jgensub -h|--help

Submit a job after processing it.

Where:
-j <jobfile>        File containing job script and jobgen directives.
-p <profname>       Profile to load.
--version           Print version.
<jobgen-args>       Supplemental arguments to pass directly to jobgen.

Options:
-c <host>           Submit on remote host.
--dry-run           Generate and show job file but do not submit.""")

#
# main
#

if __name__ == "__main__":
    try:
        directives = None
        dryrun = False
        host = None
        jobfile = None
        profname = None

        args = sys.argv[1:]
        while args:
            arg = args.pop(0)

            if arg == "-c":
                host = args.pop(0)
            elif arg == "--dry-run":
                dryrun = True
            elif arg in ["-h", "--help"]:
                print_usage()
                sys.exit(0)
            elif arg == "-j":
                jobfile = args.pop(0)
            elif arg == "-p":
                profname = args.pop(0)
            elif arg == "--version":
                print(VERSION)
                sys.exit(0)
            elif arg == "--":
                directives = args[:]
                del args[:]
            else:
                raise Exception

        if jobfile == None:
            raise Exception()
    except SystemExit:
        raise
    except:
        traceback.print_exc()
        stderr.write("error: bad/missing arguments\n")
        sys.exit(1)

    pargs = ["jobgen", "-j", jobfile]
    if profname:
        pargs.extend(["-p", profname])
    if directives:
        pargs.extend(directives)

    p = subprocess.Popen(pargs, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    sout, serr = p.communicate()
    if p.returncode != 0:
        stderr.write("error: failed to run jobgen (%s)" % serr)
        sys.exit(1)

    jobgenout = sout

    pargs = []
    if host:
        pargs.extend(["ssh", host])
    if dryrun:
        pargs.extend(["cat", "-"])
    else:
        pargs.extend(["qsub", "-"])
    p = subprocess.Popen(pargs, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    sout, serr = p.communicate(jobgenout)
    if p.returncode != 0:
        stderr.write("error: failed to submit (%s)" % serr)
        sys.exit(1)

    stdout.write(sout)
    sys.exit(0)
