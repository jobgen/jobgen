#! /usr/bin/env python3
#
# jobgen.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

# disable writing bytecode (uses encoding hack)
import sys as _sys
_sys.dont_write_bytecode = True

from configparser import ConfigParser
import json
import logging
import os
import os.path
import pprint
import sys
from sys import stderr, stdin
import traceback

from jobgen.pyerrors.errors import Error, is_error

from jobgen.constants import PREFIX, VERSION
from jobgen import globs
from jobgen import lib
from jobgen.generators import get_generator, list_generators
from jobgen.generators.base import GeneratorException
from jobgen.hooks import HookException, run_hooks
from jobgen.profile import get_directives, list_profiles, load_profiles, show_profile
from jobgen.reqinfo import RequestInfo, show_meta, show_request, show_request_flat
from jobgen.resources import ResourceException

def setup_logger():
    if globs.debug:
        loglevel = logging.DEBUG
    elif globs.verbose:
        loglevel = logging.INFO
    else:
        loglevel = logging.WARNING
    logging.basicConfig(format="%(levelname)s: %(message)s", level=loglevel)
    globs.logger = logging.getLogger()
    logging.addLevelName(logging.CRITICAL, "critical")
    logging.addLevelName(logging.ERROR, "error")
    logging.addLevelName(logging.WARNING, "warning")
    logging.addLevelName(logging.INFO, "info")
    logging.addLevelName(logging.DEBUG, "debug")

def setup():
    setup_logger()

    globs.whoami_username, globs.whoami_groupname, globs.whoami_groupnames = lib.whoami_all()

    binfile = os.path.realpath(sys.argv[0])
    globs.homedir = os.path.realpath(os.path.join(os.path.dirname(binfile), "../.."))
    globs.bindir = os.path.join(globs.homedir, "bin")

    globs.sysprofilesdir = os.path.join(globs.homedir, "etc/jobgen/profiles")
    globs.userprofilesdir = os.path.expanduser("~/.jobgen/profiles")

    globs.syshooksdir = os.path.join(globs.homedir, "lib/jobgen/jobgen/hooks")
    globs.userhooksdir = os.path.expanduser("~/.jobgen/hooks")

    globs.conf = ConfigParser()
    globs.conf.optionxform = str
    paths = [
        os.path.join(globs.homedir, "etc/jobgen/jobgen.conf"),
        os.path.expanduser("~/.jobgen/jobgen.conf"),
    ]
    for path in paths:
        if os.path.exists(path):
            globs.conf.read(path)

def print_usage():
    print("""\
usage: jobgen [<options>] [-p <profile>] -j <jobfile>
       jobgen [-p <profile>] [-j <jobfile>] --show-meta[=<fmt>]
       jobgen [-p <profile>] [-j <jobfile>] --show-profile[=<fmt>]
       jobgen [-p <profile>] [-j <jobfile>] --show-profile-flat
       jobgen [<options>] [-p <profile>] --show-request[=<fmt>] -j <jobfile>
       jobgen -l|--list
       jobgen --version
       jobgen -h|--help

Generate job file targeted for specific queueing system.

Where:
-j <jobfile>        Job file.
-l|--list           List profiles.
-p <profile>        Profile to load.
--show-meta[=<fmt>] Show meta information. <fmt> is one of "text", "json".
                    Default is "text".
--show-profile[=<fmt>]
                    Show profile. <fmt> is one of "text", "json". Default is
                    "text".
--show-profile-flat
                    Show flattened (no sections) profile. json format only.
--show-request[=<fmt>]
                    Show request information. <fmt> is one of "text", "json".
                    Default is "text".
--version           Print version.

Options:
-c <name>=<value>   Request chunk setting.
-H <name>=<value>   Request hook setting.
-k <name>=<value>   Setting using full key name.
-r <name>=<value>   Request attribute setting.
-v <name>=<value>   Request environment variable.
--enforce y|n       Enforce property constraints. Default is y.

--debug             Enable debug.
--verbose           Enable verbosity.""")

if __name__ == "__main__":
    try:
        cldirectives = []
        enforce = True
        jobfile = None
        listgenerators = False
        listprofiles = False
        profname = None
        showmetafmt = None
        showprofilefmt = None
        showrequestfmt = None
        showrequestflatfmt = None

        args = sys.argv[1:]
        while args:
            arg = args.pop(0)

            if arg == "--enforce":
                enforce = args.pop(0)
                if enforce not in ["n", "y"]:
                    stderr.write("error: enforce must be y or n\n")
                    sys.exit(1)
                enforce = enforce == "y"
            elif arg == "-j" and args:
                jobfile = args.pop(0)
            elif arg in ["-c", "-C", "-H", "-k", "-r", "-R", "-v"] and args:
                cldirectives.extend([arg, args.pop(0)])
            elif arg in ["-l", "--list"]:
                listprofiles = True
                break
            elif arg == "--list-generators":
                listgenerators = True
            elif arg == "-p" and args:
                profname = args.pop(0)
            elif arg == "--show-meta" or arg.startswith("--show-meta="):
                if "=" not in arg:
                    arg = "%s=text" % arg
                _, fmt = arg.split("=", 1)
                if fmt in ["json", "text"]:
                    showmetafmt = fmt
                else:
                    stderr.write("error: unknown format for show meta\n")
                    sys.exit(1)
            elif arg == "--show-profile" or arg.startswith("--show-profile="):
                if "=" not in arg:
                    arg = "%s=text" % arg
                _, fmt = arg.split("=", 1)
                if fmt in ["json", "text"]:
                    showprofilefmt = fmt
                else:
                    stderr.write("error: unknown format for show profile\n")
                    sys.exit(1)
            elif arg == "--show-request" or arg.startswith("--show-request="):
                if "=" not in arg:
                    arg = "%s=text" % arg
                _, fmt = arg.split("=", 1)
                if fmt in ["json", "text"]:
                    showrequestfmt = fmt
                else:
                    stderr.write("error: unknown format for show request\n")
                    sys.exit(1)
            elif arg == "--show-request-flat" or arg.startswith("--show-request-flat="):
                if "=" not in arg:
                    arg = "%s=json" % arg
                _, fmt = arg.split("=", 1)
                if fmt in ["json", "text"]:
                    showrequestflatfmt = fmt
                else:
                    stderr.write("error: unknown format for show request flat\n")
                    sys.exit(1)

            elif arg == "--debug":
                globs.debug = True
            elif arg in ["-h", "--help"]:
                print_usage()
                sys.exit(0)
            elif arg == "--verbose":
                globs.verbose = True
            elif arg == "--version":
                print(VERSION)
                sys.exit(0)
            else:
                #print("arg", arg)
                raise Exception()

        if showmetafmt or showprofilefmt or showrequestfmt or showrequestflatfmt or listgenerators or listprofiles:
            pass
        else:
            if None in [jobfile]:
                raise Exception()
    except SystemExit:
        raise
    except:
        if globs.debug:
            stderr.write("traceback (%s)\n" % traceback.format_exc())
        stderr.write("error: bad/missing arguments\n")
        sys.exit(1)

    try:
        setup()
    except:
        #traceback.print_exc()
        stderr.write("error: bad system/user configuration\n")
        sys.exit(1)

    try:
        if listprofiles:
            list_profiles()
            sys.exit(0)
    except SystemExit:
        raise
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        globs.logger.error("failed to list profiles")
        sys.exit(1)

    try:
        if listgenerators:
            list_generators(profname)
            sys.exit(0)
    except SystemExit:
        raise
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        globs.logger.error("failed to list generators")
        sys.exit(1)

    #
    # collect directives and unused lines; update profile
    #
    try:
        jfdirectives = None
        jgdirectives = None

        if jobfile:
            if jobfile == "-":
                jobf = stdin
            else:
                jobf = open(jobfile)
            joblines = []
            for line in jobf.readlines():
                if line.endswith("\n"):
                    line = line[:-1]
                joblines.append(line)
            jfdirectives, ulines = get_directives(PREFIX, joblines)
        else:
            jfdirectives = []
            ulines = []
        jgdirectives = jfdirectives+cldirectives

    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        globs.logger.error("failed to parse directives")
        sys.exit(1)

    #
    # load profiles
    #
    try:
        prof = load_profiles(profname)
        if jgdirectives:
            prof.load_directives(jgdirectives)
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        globs.logger.error("failed to load profiles")
        sys.exit(1)

    if showprofilefmt:
        show_profile(prof, showprofilefmt)
        sys.exit(0)

    #
    # build reqinfo
    #
    try:
        # extract special settings: request.queue and request.qs
        secnames = [
            "default",
            "group.%s" % globs.whoami_groupname,
            "user.%s" % globs.whoami_username,
            "directives",
        ]
        reqinfo = RequestInfo()
        reqinfo.load(prof.get_settings(secnames), False)

        qname = reqinfo.get("request.queue")
        qsname = reqinfo.get("request.qs")
        #print("qname (%s) qsname (%s)" % (qname, qsname))
        if None in [qname, qsname]:
            globs.logger.error("cannot find queue (%s) and/or queueing system (%s) in profiles" % (qname, qsname))
            sys.exit(1)

        # build request info object
        secnames = [
                "default",
                "qs.%s" % qsname,
                "queue.%s" % qname,
                "group.%s" % globs.whoami_groupname,
                "user.%s" % globs.whoami_username,
                "directives",
        ]
        #print(secnames)
        reqinfo = RequestInfo()
        reqinfo.load(prof.get_settings(secnames), enforce)
    except SystemExit:
        raise
    except ResourceException as e:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        globs.logger.error("failed to set up request: %s" % e)
        sys.exit(1)
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        globs.logger.error("failed to set up request")
        sys.exit(1)

    if showmetafmt and jobfile == None:
        show_meta(reqinfo, showmetafmt)
        sys.exit(0)

    if showrequestfmt and jobfile == None:
        show_request(reqinfo, showrequestfmt)
        sys.exit(0)

    if showrequestflatfmt and jobfile == None:
        show_request_flat(reqinfo, showrequestflatfmt)
        sys.exit(0)

    #
    # run hooks
    #
    try:
        run_hooks(reqinfo, enforce)
    except HookException as e:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        globs.logger.error("hook failed: %s" % e.message)
        sys.exit(1)
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        globs.logger.error("failed to run hooks")
        sys.exit(1)

    if showmetafmt:
        show_meta(reqinfo, showmetafmt)
        sys.exit(0)

    if showrequestfmt:
        show_request(reqinfo, showrequestfmt)
        sys.exit(0)

    if showrequestflatfmt:
        show_request_flat(reqinfo, showrequestflatfmt)
        sys.exit(0)

    #
    # convert to target qs
    #
    try:
        gen = get_generator(reqinfo)
        if not gen:
            globs.logger.error("unknown generator")
            sys.exit(1)

        lines = err = gen.generate_jobfile(ulines)
        if is_error(err):
            stderr.write("%s\n" % err)
            sys.exit(1)
        print("\n".join(lines))
    except SystemExit:
        raise
    except GeneratorException as e:
        globs.logger.error("failed to generate new job file: %s" % e)
        sys.exit(1)
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        globs.logger.error("failed to generate new job file")
        sys.exit(1)

    if globs.debug:
        for x in sorted(reqinfo.protod.items()):
            print(x)
        print("---------------------")
        for x in sorted(reqinfo.maind.items()):
            print(x)
