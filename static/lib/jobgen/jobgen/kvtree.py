#! /usr/bin/env python3
#
# jobgen/kvtree.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

class KVTreeDict(dict):
    pass

class KVTreeNone:
    pass

class KVTreeException(Exception):
    pass

class KVTree:
    """Key-value collection organized as a tree with support for
    partial and wlicard match.

    A tree organization, where each component on the key
    (dot-separated), exists as key in a dictionary at a specific
    level.

    E.g., a key of x.y.z is
    { "x":
        { "y":
            { "z": <value> }
        }
    }

    The pros and cons are when compared with a simple dictionary:
    + search for partial subkeys is faster and trivial
    + wildcard search is faster and trivial
    - create and delete are longer
    - get and set are longer

    As such, the main optimization of the kvtree is for search of
    partial keys and wildcards. All other operations are typically
    slower.
    """

    def __init__(self):
        self.d = KVTreeDict()

    def get(self, k, default=None):
        """Get single value for k. Exact matches only.
        """
        d = self.d
        comps = k.split(".")
        for comp in comps:
            if not isinstance(d, KVTreeDict):
                return default
            if comp in d:
                d = d[comp]
            else:
                return default
        if isinstance(d, KVTreeDict):
            return default
        return d

    def has_key(self, k):
        return self.get(k, KVTreeNone) != KVTreeNone

    def keys(self):
        return [k for k, _ in self.items()]

    def items(self):
        def walk(d, pref=None):
            l = []
            for k, v in d.items():
                if pref == None:
                    subk = k
                else:
                    subk = "%s.%s" % (pref, k)
                if isinstance(v, KVTreeDict):
                    l.extend(walk(v, subk))
                else:
                    l.append((subk, v))
            return l

        return walk(self.d)

    def match(self, k, default=None):
        """Get values that match k with wildcard support.

        E.g., given:
            request.chunk.*.*.ncores = <value>
            request.chunk.*.default.ncores = <value>
            request.chunk.0.default.ncores = <value>
        a request for:
            request.chunk.0.default.ncores
        will return all the values, from general to specific (i.e.,
        wildcards match first).

        Given such an ordered list of values, the values may be used
        as needed. E.g., if the values are dicts, they can be
        combined so that a final dict results with the latter values
        overwriting the former.
        """
        def walk(d, comps):
            if not comps:
                # return as a values
                return [d]

            comp = comps[0]
            if not isinstance(d, KVTreeDict):
                raise KVTreeException("reached end of kvtree")

            values = []
            if "*" in d or comp in d:
                if "*" in d:
                    v = walk(d["*"], comps[1:])
                    if v:
                        values.extend(v)
                if comp in d:
                    v = walk(d[comp], comps[1:])
                    if v:
                        values.extend(v)
            return values

        try:
            comps = k.split(".")
            values = walk(self.d, comps)
            if len(values) == 0:
                return default
            return values
        except:
            raise KVTreeException("could not get values")

    def set(self, k, v):
        try:
            d = self.d
            comps = k.split(".")
            for comp in comps[:-1]:
                d = d.setdefault(comp, KVTreeDict())
                if not isinstance(d, KVTreeDict):
                    raise KVTreeException("value already set for component (%s)" % (comp,))
            lastcomp = comps[-1]
            if lastcomp in d:
                raise KVTreeException("value already set for component" % (lastcomp,))

            d[lastcomp] = v
        except:
            raise KVTreeException("unexpected problem")

    def setdefault(self, k, v):
        vv = self.get(k, KVTreeNone)
        if vv != KVTreeNone:
            return vv
        else:
            self.set(k, v)
            return v

    def wget(self, k, default=None):
        """Get single value for k with wildcard support. Exact matches
        for components are used over wildcard matches.

        E.g., given:
            request.chunk.*.*.ncores = <value>
            request.chunk.*.default.memory = <value>
            request.chunk.*.default.ncores = <value>
        a request for:
            request.chunk.0.default.ncores
        will return the value associated with:
            request.chunk.*.default.ncores
        """
        def walk(d, comps):
            if not comps:
                # return "d" as the terminal value
                return d

            if not isinstance(d, KVTreeDict):
                raise KVTreeException("reached end of kvtree")

            comp = comps[0]
            try:
                return walk(d[comp], comps[1:])
            except:
                pass
            return walk(d["*"], comps[1:])

        try:
            comps = k.split(".")
            return walk(self.d, comps)
        except:
            traceback.print_exc()
            return default
