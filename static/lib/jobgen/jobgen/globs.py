#! /usr/bin/env python3
#
# jobgen/globs.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

conf = None

homedir = None
bindir = None
libdir = None

sysprofilesdir = None
userprofilesdir = None
syshooksdir = None
userhooksdir = None

debug = False
verbose = False

logger = None

whoami_username = None
whoami_groupname = None
whoami_groupnames = None
