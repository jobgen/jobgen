#! /usr/bin/env python3
#
# jobgen/resources.py
#
# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import copy
import traceback

from jobgen import globs
from jobgen import lib
from jobgen import libconvert
from jobgen.libconvert import ValueException
from jobgen.kvtree import KVTree

class ResourceException(Exception):
    pass

class UnsetValue:
    pass

def isset(o):
    return not isunset(o)

def isunset(o):
    return isinstance(o, UnsetValue)

class Resource:

    def __init__(self, enforce=True):
        self.enforce = enforce
        self.props = {
            "description": UnsetValue(),
            "keytype": UnsetValue(),
            "scalable": UnsetValue(),
        }
        self.value = UnsetValue()

    def __repr__(self):
        propinfo = " ".join(["%s=%s" % (k, v) for k,v in sorted(self.props.items()) if isset(v)])
        valueinfo = isunset(self.value) and "none" or self.value

        return "<%s props=%s value=%s>" % (self.__class__.__name__, propinfo, valueinfo)

    def clone(self):
        """Make a copy of the current object.
        """
        return copy.deepcopy(self)

    def inherit(self, r):
        """Update property settings from given object.
        """
        for name, value in r.props.items():
            if isset(value):
                self.props[name] = copy.deepcopy(value)

    def get(self):
        """Get normalized value.
        """
        return self.value

    def getprop(self, name, default=None):
        return self.props.get(name)

    def hasprop(self, name):
        return isset(self.props[name])

    def set(self, value):
        """Set normalized value. Must satisfy resource-specific
        constraints as set in properties.
        """
        self.value = value

    def set_enforce(self, enforce):
        self.enforce = enforce

    def sget(self):
        """Get value as string. Suitable for use with sset().
        """
        return str(self.value)

    def sgetprop(self, name):
        """Return property value as string suitable for ssetprop().
        """
        props = self.props

        if name == "scalable":
            return libconvert.convert_boolean2truefalse(props[name])
        elif name in props:
            return str(props[name])
        else:
            raise ResourceException("unknown property (%s)" % str(name))

    def sset(self, svalue):
        """Set value using string value.
        """
        self.set(svalue)

    def ssetprop(self, name, svalue):
        """Set property using string value.
        """
        props = self.props

        if name in ["description", "keytype"]:
            props[name] = libconvert.convert_string(svalue)
        elif name == "scalable":
            props[name] = libconvert.convert_boolean(svalue)
        else:
            raise ResourceException("unknown property (%s)" % str(name))

class BooleanResource(Resource):

    def __init__(self):
        Resource.__init__(self)

    def set(self, value):
        # test against constraints
        if value in [False, True]:
            self.value = value
        else:
            raise ResourceException("bad value (%s)" % str(value))

    def sget(self):
        return libconvert.convert_boolean2truefalse(self.value)

    def sset(self, svalue):
        value = libconvert.convert_boolean(svalue)
        self.set(value)

    def ssetprop(self, name, svalue):
        props = self.props

        if name not in props:
            raise ResourceException("unknown property (%s)" % str(name))
        else:
            Resource.ssetprop(self, name, svalue)

class FirstLastRangeResource(Resource):

    def __init__(self, enforce=True):
        Resource.__init__(self, enforce)
        self.props.update({
            "max": UnsetValue(),
            "maxcount": UnsetValue(),
            "min": UnsetValue()
        })

    def set(self, value):
        props = self.props

        if self.enforce:
            first, last, step = value
            if first > last:
                raise ResourceException("first (%s) exceeds last (%s)" % (first, last))
            if step <= 0:
                raise ResourceException("step (%s) less than 1" % step)

            if isset(props["max"]):
                maxvalue = props["max"]
                if first > maxvalue:
                    raise ResourceException("first (%s) more than max (%s)" % (first, maxvalue))
                if last > maxvalue:
                    raise ResourceException("last (%s) more than max (%s)" % (last, maxvalue))
            if isset(props["min"]):
                minvalue = props["min"]
                if first < minvalue:
                    raise ResourceException("first (%s) less than min (%s)" % (first, minvalue))
                if last < minvalue:
                    raise ResourceException("last (%s) more than min (%s)" % (last, minvalue))
            if isset(props["maxcount"]):
                maxcount = props["maxcount"]
                count = last-first+1
                if count > maxcount:
                    raise ResourceException("number of elements (%s) more than maxcount (%s)" % (count, maxcount))

        self.value = value

    def sget(self):
        return "%d-%d:%d" % self.value

    def sset(self, svalue):
        value = libconvert.convert_firstlastrange2tuple(svalue)
        self.set(value)

    def ssetprop(self, name, svalue):
        props = self.props

        if name in ["max", "maxcount", "min"]:
            props[name] = int(svalue)
        else:
            Resource.ssetprop(self, name, svalue)

class FloatResource(Resource):

    def __init__(self):
        Resource.__init__(self)
        self.props.update({
            "max": UnsetValue(),
            "min": UnsetValue()
        })

    def set(self, value):
        props = self.props

        if self.enforce:
            if isset(props["max"]) and value > props["max"]:
                raise ResourceException("value (%s) more than max (%s)" % (value, props["max"]))
            if isset(props["min"]) and value < props["min"]:
                raise ResourceException("value (%s) less than min (%s)" % (value, props["min"]))
        self.value = value

    def sset(self, svalue):
        value = libconvert.convert_float(svalue)
        self.set(value)

    def ssetprop(self, name, svalue):
        props = self.props

        if name in ["max", "min"]:
            props[name] = libconvert.convert_float(svalue)
        else:
            Resource.ssetprop(self, name, svalue)

class IntegerResource(Resource):

    def __init__(self):
        Resource.__init__(self)
        self.props.update({
            "max": UnsetValue(),
            "min": UnsetValue()
        })

    def set(self, value):
        props = self.props

        if self.enforce:
            if isset(props["max"]) and value > props["max"]:
                raise ResourceException("value (%s) more than max (%s)" % (value, props["max"]))
            if isset(props["min"]) and value < props["min"]:
                raise ResourceException("value (%s) less than min (%s)" % (value, props["min"]))
        self.value = value

    def sset(self, svalue):
        value = libconvert.convert_integer(svalue)
        self.set(value)

    def ssetprop(self, name, svalue):
        props = self.props

        if name in ["max", "min"]:
            props[name] = libconvert.convert_integer(svalue)
        else:
            Resource.ssetprop(self, name, svalue)

class MemoryResource(Resource):

    def __init__(self):
        Resource.__init__(self)
        self.props.update({
            "max": UnsetValue(),
            "min": UnsetValue()
        })

    def set(self, value):
        props = self.props

        if self.enforce:
            if isset(props["max"]) and value > props["max"]:
                raise ResourceException("value (%s) more than max (%s)" % (value, props["max"]))
            if isset(props["min"]) and value < props["min"]:
                raise ResourceException("value (%s) less than min (%s)" % (value, props["min"]))
        self.value = value

    def sget(self):
        return "%s" % libconvert.convert_bytes2memoryfmt(self.value, "*")

    def sgetprop(self, name):
        props = self.props

        if name in ["max", "min"]:
            return libconvert.convert_bytes2memoryfmt(props[name], "*")
        else:
            return Resource.sgetprop(self, name)

    def sset(self, svalue):
        value = libconvert.convert_memory(svalue, "B")
        self.set(value)

    def ssetprop(self, name, svalue):
        props = self.props

        if name in ["max", "min"]:
            props[name] = libconvert.convert_memory(svalue, "B")
        else:
            Resource.ssetprop(self, name, svalue)

class NameListResource(Resource):

    def __init__(self):
        Resource.__init__(self)

    def set(self, value):
        self.value = value

    def sget(self):
        return ",".join(self.value)

    def sset(self, svalue):
        try:
            self.set(svalue.split(","))
        except:
            raise ResourceException("bad value (%s)" % str(svalue))

    def ssetprop(self, name, svalue):
        Resource.ssetprop(self, name, svalue)

class ReferenceResource(Resource):

    def __init__(self):
        Resource.__init__(self)

    def set(self, value):
        self.value = str(value)

    def sset(self, svalue):
        self.set(svalue)

    def ssetprop(self, name, svalue):
        Resource.ssetprop(self, name, svalue)

class StringResource(Resource):

    def __init__(self):
        Resource.__init__(self)

    def set(self, value):
        self.value = str(value)

    def sset(self, svalue):
        if not isinstance(svalue, str):
            raise ResourceException("bad value (%s)" % str(svalue))
        self.set(svalue)

    def ssetprop(self, name, svalue):
        Resource.ssetprop(self, name, svalue)

class StringMatchResource(Resource):

    def __init__(self):
        Resource.__init__(self)
        self.props.update({
            "values": UnsetValue(),
        })

    def sgetprop(self, name):
        props = self.props

        if name in ["values"]:
            return ",".join(props[name])
        else:
            return Resource.sgetprop(self, name)

    def set(self, value):
        props = self.props

        if self.enforce:
            if isset(props["values"]) and value not in props["values"]:
                raise ResourceException("value (%s) not in values (%s)" % (value, props["values"]))
        self.value = value

    def sset(self, svalue):
        if not isinstance(svalue, str):
            raise ResourceException("bad value (%s)" % str(svalue))
        self.set(svalue)

    def ssetprop(self, name, svalue):
        props = self.props

        if name == "values":
            props["values"] = libconvert.convert_csv(svalue)
        else:
            Resource.ssetprop(self, name, svalue)

class TimeResource(Resource):

    def __init__(self):
        Resource.__init__(self)
        self.props.update({
            "max": UnsetValue(),
            "min": UnsetValue()
        })

    def set(self, value):
        props = self.props

        if self.enforce:
            if isset(props["max"]) and value > props["max"]:
                raise ResourceException("value (%s) more than max (%s)" % (value, props["max"]))
            if isset(props["min"]) and value < props["min"]:
                raise ResourceException("value (%s) less than min (%s)" % (value, props["min"]))
        self.value = value

    def sget(self):
        return libconvert.convert_seconds2timefmt(self.value, "*")

    def sgetprop(self, name):
        props = self.props

        if name in ["max", "min"]:
            return libconvert.convert_seconds2timefmt(props[name], "*")
        else:
            return Resource.sgetprop(self, name)

    def sset(self, svalue):
        value = libconvert.convert_time2seconds(svalue)
        self.set(value)

    def ssetprop(self, name, svalue):
        props = self.props

        if name in ["max", "min"]:
            props[name] = libconvert.convert_time2seconds(svalue)
        else:
            Resource.ssetprop(self, name, svalue)

rtype2resource = {
    "boolean": BooleanResource,
    "firstlastrange": FirstLastRangeResource,
    "float": FloatResource,
    "integer": IntegerResource,
    "memory": MemoryResource,
    "namelist": NameListResource,
    "reference": ReferenceResource,
    "string": StringResource,
    "stringmatch": StringMatchResource,
    "time": TimeResource,
}

resource2rtype = dict([(v, k) for k,v in rtype2resource.items()])

class ResourceFactory:

    def __init__(self):
        pass

    def create_resource(self, rtype):
        rclass = rtype2resource.get(rtype)
        if rclass == None:
            raise ResourceException("unknown resource type (%s)" % str(rtype))
        return rclass()

    def run(self, settings, enforce=True):
        """Resolve all lines. Return the result as a dictionary of key=value.

        protod - dictionary of prototypes for "meta" keys
        maind - dictionary of non-prototypes for non-"meta" keys
        """
        protod = KVTree()
        maind = KVTree()

        for k, svalue in settings:
            if k.startswith("meta."):
                _, propname, sk = k.split(".", 2)
                if propname == "type":
                    if protod.has_key(sk):
                        raise ResourceException("resource already defined for key (%s)" % k)
                    pr = protod.setdefault(sk, self.create_resource(svalue))
                else:
                    pr = protod.get(sk)
                    if pr == None:
                        pr = protod.wget(sk)
                        if pr == None:
                            for x in sorted(protod.items()):
                                print(x)
                            raise ResourceException("resource not defined for key (%s)" % k)
                        else:
                            # clone the wildcard match and store the
                            # new prototype under the subkey
                            pr = pr.clone()
                            protod.set(sk, pr)
                    pr.ssetprop(propname, svalue)
        for k, svalue in settings:
            if not k.startswith("meta."):
                r = maind.get(k)
                if r == None:
                    prs = protod.match(k)
                    if not prs:
                        raise ResourceException("resource not defined for key (%s)" % str(k))
                    for pr in prs:
                        if not isinstance(pr, Resource):
                            raise ResourceException("found non-resource for key (%s)" % str(k))
                    r = prs[0].clone()
                    for pr in prs[1:]:
                        r.inherit(pr)
                    maind.set(k, r)
                r.set_enforce(enforce)
                try:
                    r.sset(svalue)
                except (ResourceException, ValueException) as e:
                    raise ResourceException("key (%s) %s" % (k, e))
        return protod, maind
