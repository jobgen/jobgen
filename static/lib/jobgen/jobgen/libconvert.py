#! /usr/bin/env python3
#
# jobgen/libconvert.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import re
import traceback

from jobgen import globs
from jobgen import lib

MEMORY_UNIT2BYTES = {
    "B": 1,
    "K": 1024,
    "KB": 1024,
    "M": 1024**2,
    "MB": 1024**2,
    "G": 1024**3,
    "GB": 1024**3,
    "T": 1024**4,
    "TB": 1024**4,
    "P": 1024*5,
    "PB": 1024*5,
}

FIRSTLASTRANGE_RE = "^(?P<first>\d+)-(?P<last>\d+)(:(?P<step>\d+))?$"
FIRSTLASTRANGE_CRE = re.compile(FIRSTLASTRANGE_RE)
STRING_RE = """^(?P<quote>\"\"\"|'''|"|')(?P<str>.*)(?P=quote)$"""
STRING_CRE = re.compile(STRING_RE)
STRINGLIST_STRING_RE = """(?P<quote>\"\"\"|'''|"|')(?P<str>.*?)(?P=quote)"""
STRINGLIST_STRING_CRE = re.compile(STRINGLIST_STRING_RE)

TIME_FIELDMULTS = [1, 60, 3600, 86400]

class ValueException(Exception):
    pass

def convert_boolean(s):
    try:
        if s in ["true", "y", "yes"]:
            return True
        elif s in ["false", "n", "no"]:
            return False
        else:
            raise Exception()
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        raise ValueException("value (%s) not a boolean" % (s,))

def convert_boolean2truefalse(value):
    if value == True:
        return "true"
    elif value == False:
        return "false"
    else:
        raise ValueException("value (%s) not a boolean" % (value,))

def convert_bytes2memoryfmt(value, tofmt="B"):
    try:
        tofmts = ["B", "K", "M", "G", "T", "P"]

        if tofmt == "*":
            # find biggest unit without loss of accuracy
            for i, tofmt in enumerate(tofmts):
                if value % MEMORY_UNIT2BYTES[tofmt] != 0:
                    tofmt = tofmts[i-1]
                    break
            else:
                tofmt = "B"
        value = int(value/MEMORY_UNIT2BYTES[tofmt])
        return "%s%s" % (value, tofmt)
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        raise ValueException("value (%s) does not convert to format (%s)" % (value, tofmt))

def convert_csv(s):
    try:
        return [x.strip() for x in s.split(",")]
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        raise ValueException("value (%s) not a csv" % (s,))

def convert_float(s):
    try:
        return float(s)
    except:
        raise ValueException("value (%s) not a float" % (s,))

def convert_integer(s):
    try:
        return int(s)
    except:
        raise ValueException("value (%s) not an integer" % (s,))

def convert_memory(s, tounit, defunit="M"):
    try:
        if s[-1].isdigit():
            unit = defunit
        elif s[-2].isdigit():
            unit = s[-1:]
            s = s[:-1]
        else:
            unit = s[-2:]
            s = s[:-2]
        return int((int(s)*MEMORY_UNIT2BYTES[unit])/MEMORY_UNIT2BYTES[tounit])
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        raise ValueException("value (%s) not a memory" % (s,))

def convert_firstlastrange2tuple(s):
    try:
        m = FIRSTLASTRANGE_CRE.match(s)
        d = m.groupdict()
        step = d.get("step")
        step = int(step == None and "1" or step)
        return (int(d["first"]), int(d["last"]), step)
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        raise ValueException("value (%s) is not a firstlastrange (first-last:step)" % s)

def convert_seconds2timefmt(value, tofmt="s"):
    try:
        if tofmt == "*":
            if value > 3600:
                tofmt = "h:m:s"
            elif value > 60:
                tofmt = "m:s"
            else:
                tofmt = "s"

        if tofmt == "s":
            return str(value)
        elif tofmt == "m:s":
            minutes, rem = divmod(value, 60)
            return "%s:%0.2d" % (minutes, rem)
        elif tofmt == "h:m:s":
            hours, rem = divmod(value, 3600)
            minutes, rem = divmod(rem, 60)
            return "%s:%0.2d:%0.2d" % (hours, minutes, rem)
        elif tofmt == "d:h:m:s":
            days, rem = divmod(value, 86400)
            hours, rem = divmod(rem, 3600)
            minutes, rem = divmod(rem, 60)
            return "%s:%0.2d:%0.2d:%0.2d" % (days, hours, minutes, rem)
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        raise Exception("value (%s) does not convert to format (%s)" % (value, tofmt))

def convert_string(s):
    try:
        m = STRING_CRE.match(s)
        if not m:
            return str(s)
        return m.group("str")
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        raise ValueException("value (%s) not a string" % (s,))

def convert_stringlist(s):
    """Comma-separated list of quoted strings (single and triple
    quotes of single and double-quotes). Returns list of strings.
    """
    try:
        values = []
        while s:
            m = STRINGLIST_STRING_CRE.match(s)
            if m:
                values.append(m.group("str"))
                _, pos = m.span()
                nextch = s[pos:pos+1]
                if nextch and nextch != ",":
                    raise Exception()
                s = s[pos+1:]
        return values
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        raise ValueException("value (%s) not a stringlist" % (s,))

def convert_time2seconds(s):
    try:
        total = 0
        fields = list(map(int, s.split(":")))
        for i, v in enumerate(fields[::-1]):
            total += TIME_FIELDMULTS[i]*v
        return total
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        raise ValueException("value (%s) not a time" % (s,))
