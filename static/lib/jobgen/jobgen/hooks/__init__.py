#! /usr/bin/env python3
#
# jobgen/hooks/__init__.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import sys
import traceback

from jobgen import globs

class HookException(Exception):
    pass

def run_hooks(reqinfo, enforce=True):
    try:
        sys.path.insert(0, globs.syshooksdir)
        sys.path.insert(0, globs.userhooksdir)

        # get syshooks and prune
        names = reqinfo.get("request.syshooks.names", [])[:]
        allow = set(reqinfo.get("request.syshooks.allow", []))
        deny = set(reqinfo.get("request.syshooks.deny", []))

        if allow:
            names = [name for name in names if name in allow]
        if deny:
            names = [name for name in names if name not in deny]

        # extend with user-specified hooks and prune
        names.extend(reqinfo.get("request.hooks.names", []))
        allow = set(reqinfo.get("request.hooks.allow", []))
        deny = set(reqinfo.get("request.hooks.deny", []))

        if allow:
            names = [name for name in names if name in allow]
        if deny:
            names = [name for name in names if name not in deny]
        if not names:
            return

        for name in names:
            try:
                mod = __import__("syshooks", None, None, [name], 0)
                if name not in mod.__dict__:
                    mod = __import__("userhooks", None, None, [name], 0)
                    if name not in mod.__dict__:
                        raise HookException("cannot find hook (%s)" % (name,))
            except:
                globs.logger.debug("traceback (%s)" % traceback.format_exc())
                raise HookException("cannot find/load hook (%s)" % (name,))

            try:
                globs.logger.debug("running hook (%s) mod (%s) file (%s)\n" \
                        % (name, mod.__name__, mod and mod.__file__))
                mod.__dict__[name](reqinfo)
            except Exception as e:
                globs.logger.debug("traceback (%s)" % traceback.format_exc())
                raise HookException("hook (%s) reason (%s)" % (name, e))
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        if enforce:
            raise
    finally:
        del sys.path[0]
        del sys.path[0]
