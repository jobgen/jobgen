#! /usr/bin/env python3
#
# lib/jobgen/hooks/default.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import traceback

from jobgen import globs
from jobgen import libconvert
from jobgen.generators.base import get_nodegroups
from jobgen.hooks import HookException

def identity(reqinfo):
    pass

def set_tmpfs(reqinfo):
    """Set minimum tmpfs.
    """
    mintmpfs = 512*1024*1024
    nodegroups = get_nodegroups(reqinfo)
    for chunkid, nodegroup in enumerate(nodegroups):
        tmpfs = reqinfo.get("request.chunk.%s.%s.tmpfs" % (chunkid, nodegroup))
        if tmpfs in [0, None]:
            reqinfo.set("request.chunk.%s.%s.tmpfs" % (chunkid, nodegroup), mintmpfs)

def check_memory_tmpfs(reqinfo):
    """Check that memory-tmpfs a minimum amount. The minimum is
    given in request.hook.check_memory_tmpfs.mindiff.
    """
    nodegroups = get_nodegroups(reqinfo)
    for chunkid, nodegroup in enumerate(nodegroups):
        memory = reqinfo.get("request.chunk.%s.%s.memory" % (chunkid, nodegroup))
        tmpfs = reqinfo.get("request.chunk.%s.%s.tmpfs" % (chunkid, nodegroup), 0)
        diff = memory-tmpfs
        mindiff = reqinfo.get("request.hook.check_memory_tmpfs.mindiff", 0)
        if mindiff:
            if diff < mindiff:
                raise HookException("memory-tmpfs (%s) is less than minimum (%s)" \
                    % (libconvert.convert_bytes2memoryfmt(diff, "*"),
                        libconvert.convert_bytes2memoryfmt(mindiff, "*")))

def compact(reqinfo):
    """Compact request to use less chunk resources.
    """

    import math

    def _get_max_value_multipliers(reqinfo, keys):
        """Get multipliers for maxvalue/value for each key.
        """
        mults = []
        for k in keys:
            r = reqinfo.get_resource(k)
            mults.append(int(r.getprop("max")/r.get()))
        return mults

    def _get_max_keys(reqinfo, keys):
        """Get for keys that have a "max" constraint.
        """
        mkeys = []
        for k in keys:
            r = reqinfo.get_resource(k)
            if r and r.hasprop("max"):
                mkeys.append(k)
        return mkeys

    def _get_scalables(reqinfo, chunkid, nodegroup):
        skeys = []
        kre = r"request\.chunk\.%s\.%s\.[^.]*" % (chunkid, nodegroup)
        for k in reqinfo.match(kre):
            r = reqinfo.get_resource(k)
            if r.getprop("scalable") == True:
                skeys.append(k)
        return skeys

    def _scale_settings(reqinfo, keys, mult):
        """Scale all settings by mult.
        """
        for k in keys:
            r = reqinfo.get_resource(k)
            r.set(r.get()*mult)

    def compact_balanced_packed(reqinfo, method):
        """Compact the number of slots required.

        balanced - Pack multiple slots together but keep result
            balanced

        packed - pack into fewest slots possible; no requirement to
            balance.
        """
        try:
            nodegroups = get_nodegroups(reqinfo)
            for chunkid, nodegroup in enumerate(nodegroups):
                scalkeys = _get_scalables(reqinfo, chunkid, nodegroup)
                if scalkeys:
                    nslots = reqinfo.get("request.chunk.%s.%s.nslots" % (chunkid, nodegroup))
                    maxkeys = _get_max_keys(reqinfo, scalkeys)

                    diffkeys = set(scalkeys)-set(maxkeys)
                    if diffkeys:
                        globs.logger.warning("scalable resources (%s) missing max property setting\n" % ",".join(diffkeys))
                    mults = _get_max_value_multipliers(reqinfo, maxkeys)

                    if method == "balanced":
                        bitlengths = [mult.bit_length() for mult in mults]
                        blmults = [1 << (x-1) for x in bitlengths]
                        minmult = min(blmults+[nslots])
                        newnslots = nslots/minmult
                    else:
                        minmult = min(mults+[nslots])
                        newnslots = int(math.ceil(float(nslots)/minmult))

                    _scale_settings(reqinfo, scalkeys, minmult)
                    reqinfo.set("request.chunk.%s.%s.nslots" % (chunkid, nodegroup), newnslots)
        except HookException:
            globs.logger.debug("traceback (%s)" % traceback.format_exc())
            raise
        except:
            globs.logger.debug("traceback (%s)" % traceback.format_exc())
            raise HookException("compact (%s) failed" % method)

        return reqinfo

    method = reqinfo.get("request.hook.compact.method")

    if method:
        if method in ["balanced", "packed"]:
            return compact_balanced_packed(reqinfo, method)
        else:
            raise HookException("unknown compact method (%s)" % (method,))

def test(reqinfo):
    print(get_nodegroups(reqinfo))
