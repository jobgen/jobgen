#! /usr/bin/env python3
#
# jobgen/hint_processors/__init__.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

class HintException(Exception):
    pass
