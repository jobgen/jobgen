#! /usr/bin/env python3
#
# jobgen/hint_processors/hpco.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import math
import pprint
import sys
import traceback

from jobgen import globs
from jobgen.hint_processors import HintException

def get_max_value_multipliers(reqinfo, keys):
    """Get multipliers for maxvalue/value for each key.
    """
    mults = []
    for k in keys:
        o = reqinfo.getobject(k)
        mults.append(int(o.meta["max"]/o.value))
    return mults

def get_max_keys(reqinfo, keys):
    """Get for keys that have a "max" constraint.
    """
    mkeys = []
    for k in keys:
        o = reqinfo.getobject(k)
        if o and "max" in o.meta:
            mkeys.append(k)
    return mkeys

def get_scalables(reqinfo, chunkid):
    skeys = []
    kre = r"request\.chunk\.%s\.[^.]*" % chunkid
    for k in reqinfo.match(kre):
        o = reqinfo.getobject(k)
        if o.meta.get("scalable") == True:
            skeys.append(k)
    return skeys

def scale_settings(reqinfo, keys, mult):
    """Scale all settings by mult.
    """
    for k in keys:
        o = reqinfo.getobject(k)
        value = o.get()
        o.set(value*mult)

def hint_compact(reqinfo):
    return hint_compact_balanced(reqinfo)
    #return hint_compact_packed(reqinfo)

def hint_compact_balanced(reqinfo):
    """Compact the number of slots required. Pack multiple slots
    together but keep result balanced.
    """

    try:
        bitlengths = []
        nchunks = int(reqinfo.get("request.nchunks", 1))

        for chunkid in range (nchunks):
            keys = get_scalables(reqinfo, chunkid)
            if keys:
                nslots = reqinfo.get("request.chunk.%s.nslots" % chunkid)
                keys = get_max_keys(reqinfo, keys)
                mults = get_max_value_multipliers(reqinfo, keys)

                bitlengths = [mult.bit_length() for mult in mults]
                bitlengths.append(nslots.bit_length())
                blmults = [1 << (x-1) for x in bitlengths]

                minmult = min(blmults)
                scale_settings(reqinfo, keys, minmult)
                reqinfo.set("request.chunk.%s.nslots" % chunkid, nslots/minmult)
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        raise HintException("failed to apply hint_compact_balanced")

    return reqinfo

def hint_compact_packed(reqinfo):
    """Compact the number of slots required. Pack into fewest slots
    possible; no requirement to balance.
    """

    try:
        mults = []
        nchunks = int(reqinfo.get("request.nchunks", 1))

        for chunkid in range (nchunks):
            keys = get_scalables(reqinfo, chunkid)
            if keys:
                nslots = reqinfo.get("request.chunk.%s.nslots" % chunkid)
                keys = get_max_keys(reqinfo, keys)
                mults = get_max_value_multipliers(reqinfo, keys)

                minmult = min(mults)
                scale_settings(reqinfo, keys, minmult)
                newnslots = int(math.ceil(float(nslots)/minmult))
                reqinfo.set("request.chunk.%s.nslots" % chunkid, newnslots)
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
        raise HintException("failed to apply hint_compact_packed")

    return reqinfo
