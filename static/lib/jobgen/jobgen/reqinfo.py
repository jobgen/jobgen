#! /usr/bin/env python3
#
# jobgen/reqinfo.py
#
# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import json
import os.path
import re
import sys
import traceback

from jobgen import globs
from jobgen import lib
from jobgen import resources
from jobgen.resources import ReferenceResource, ResourceFactory, ResourceException, isset, resource2rtype

class RequestInfo:
    """Request information.

    Initialized with full, raw meta information. Final content
    normalized using meta information.
    """

    def __init__(self):
        self.protod = None
        self.maind = None
        self.factory = ResourceFactory()

    def __contains__(self, k):
        return self.maind.has_key(k)

    def get(self, k, default=None):
        """Return value of object.
        """
        if self.maind.has_key(k):
            r = self.maind.get(k)
            while isinstance(r, resources.ReferenceResource):
                v = r.get()
                r = self.maind.get(v)
            if r:
                return r.get()
        return default

    def get_resource(self, k, default=None):
        """Return resource object.
        """
        return self.maind.get(k, default)

    def keys(self):
        """Return list of keys for non-metadata.
        """
        return self.maind.keys()

    def load(self, settings, enforce=True):
        """Load settings and set up metadata and non-metadata
        resources.
        """
        self.protod, self.maind = self.factory.run(settings, enforce)

    def match(self, regexp):
        """Find keys that match a regexp.
        """
        try:
            matches = []
            cre = re.compile(regexp)
            for k in self.maind.keys():
                if cre.match(k):
                    matches.append(k)
        except:
            raise Exception("error trying to match keys in RequestInfo")
        return matches

    def set(self, k, v):
        """Set normalized value.
        """
        globs.logger.debug("reqinfo set k (%s) v (%s)\n" % (k, v))
        self.maind.get(k).set(v)

    def sget(self, k, default=None):
        """Return stringified version of value.
        """
        if k in self.maind.keys():
            r = self.get_resource(k)
            while isinstance(r, resources.ReferenceResource):
                v = r.get()
                r = self.get_resource(v)
            if r:
                return r.sget()
        return default

    def sset(self, k, sv):
        """Set non-normalized value.
        """
        globs.logger.debug("reqinfo sset k (%s) svalue (%s)" % (k, sv))
        self.maind[k].sset(sv)

def get_resourceinfo(resd, getprops=True, getvalue=False):
    """Get resource info as a regular json-compatible dictionary.
    """
    d = {}
    for k, pr in resd.items():
        dd = d.setdefault(k, {})
        dd["type"] = resource2rtype[pr.__class__]
        if getprops:
            for name, v in pr.props.items():
                if isset(v):
                    dd[name] = pr.sgetprop(name)
        if getvalue:
            v = pr.get()
            if isset(v):
                dd["value"] = pr.sget()
    return d

def show_meta(reqinfo, fmt):
    """Show metadata.
    """
    d = get_resourceinfo(reqinfo.protod, getprops=True, getvalue=False)
    if fmt == "json":
        print(json.dumps(d, indent=4, sort_keys=True))
    elif fmt == "text":
        for k, dd in sorted(d.items()):
            rtype = dd.pop("type")
            print("meta.type.%s = %s" % (k, rtype))
            for kk, vv in sorted(dd.items()):
                print("meta.%s.%s = %s" % (kk, k, vv))

def show_request(reqinfo, fmt):
    """Show request metadata and non-metadata.
    """
    d = get_resourceinfo(reqinfo.maind, getprops=True, getvalue=True)
    if fmt == "json":
        print(json.dumps(d, indent=4, sort_keys=True))
    elif fmt == "text":
        for k, dd in sorted(d.items()):
            for kk, vv in sorted(dd.items()):
                print("%s:%s = %s" % (k, kk, vv))

def show_request_flat(reqinfo, fmt):
    """Show request non-metadata.
    """
    d = get_resourceinfo(reqinfo.maind, getprops=False, getvalue=True)
    l = dict([(k, d["value"]) for k, d in d.items()])

    if fmt == "json":
        print(json.dumps(l, indent=4, sort_keys=True))
    elif fmt == "text":
        for k, v in sorted(l):
            print("%s = %s" % (k, v))

def show_request_flat_x(reqinfo, fmt):
    """Show request non-metadata.
    """
    d = get_resourceinfo(reqinfo.maind, getprops=False, getvalue=True)
    l = [(k, d["value"]) for k, d in d.items()]

    if fmt == "json":
        print(json.dumps(l, indent=4, sort_keys=True))
    elif fmt == "text":
        for k, v in sorted(l):
            print("%s = %s" % (k, v))
