#! /usr/bin/env python3
#
# jobgen/profile.py
#
# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import json
import os
import os.path
import traceback

from jobgen import globs

def get_directives(prefix, lines):
    directives = []
    ulines = []
    for line in lines:
        if line.startswith(prefix):
            line = line[len(prefix):].strip()
            args = split_args(line)
            while args:
                arg = args.pop(0)

                if arg in ["-c", "-H", "-k", "-r", "-R", "-v"] and args:
                    directives.extend([arg, args.pop(0)])
                else:
                    raise Exception()
        else:
            ulines.append(line)

    return directives, ulines

def split_args(line):
    # TODO: support escaped space and quotes
    args = line.split()
    return args


class Profile:
    """An ordered list of settings (as (k, v) tuples) organized by
    section. Usually loaded from a file with the format:
        [<section>]
        key = value
        ...

    Settings from multiple profile sources are concatenated, by
    section.
    """

    def __init__(self):
        self.sections = {}

    def add(self, secname, setts):
        """Add settings for a section.
        """
        secsetts = self.sections.setdefault(secname, [])
        secsetts.extend(setts)

    def get_sections(self):
        return self.sections.keys()

    def get_settings(self, secnames):
        """Concatenate settings from sections in order, by section name.
        """
        setts = []
        for secname in secnames:
            secsetts = self.sections.get(secname, [])
            setts.extend(secsetts)
        return setts

    def load(self, paths):
        """Load one or more files.
        """
        for path in paths:
            self.load_one(path)

    def load_one(self, path):
        """Load settings from file into the named sections.
        """
        try:
            f = open(path)
            globs.logger.debug("file (%s) loaded" % path)
        except:
            globs.logger.debug("file (%s) not loaded" % path)
            return

        try:
            # TODO: support multilines
            secname = None
            secsetts = None
            for line in f.readlines():
                line = line.strip()
                if line == "" or line.startswith("#"):
                    continue
                if line.startswith("[") and line.endswith("]"):
                    secname = line[1:-1]
                    secsetts = self.sections.setdefault(secname, [])
                elif secsetts == None:
                    # drop setting
                    pass
                else:
                    k, v = line.split("=", 1)
                    secsetts.append((k.strip(), v.strip()))
        except:
            globs.logger.debug("traceback (%s)" % traceback.format_exc())
            raise Exception("error loading profile (%s)" % path)
        finally:
            f.close()

    def load_directives(self, directives):
        """Load directives as settings in the "directives" section.
        Directives require processing.
        """
        dirsetts = []

        args = directives[:]
        while args:
            arg = args.pop(0)
            if arg in ["-c", "-C", "-H", "-k", "-r", "-R", "-v"]:
                qstype = None
                t = args.pop(0).split("=", 1)
                if len(t) == 2:
                    k, v = t
                else:
                    k, v = t[0], None

                if arg == "-k":
                    # full key
                    pass
                else:
                    if ":" in k:
                        qstype, k = k.split(":", 1)
                    elif arg == "-c":
                        k = "request.chunk.0.default.%s" % k
                    elif arg == "-C":
                        k = "request.chunk.%s" % k
                    if arg == "-H":
                        k = "request.hook.%s" % k
                    elif arg == "-r":
                        k = "request.%s" % k
                    elif arg == "-R":
                        pass
                    elif arg == "-v":
                        k = "request.env.%s" % k
                    if qstype:
                        k = "qs.%s.%s" % (qstype, k)

                dirsetts.append((k, v))
            else:
                raise Exception("unknown directive (%s)" % arg)

        self.add("directives", dirsetts)

def get_profile_names():
    """Get profile names.
    """
    try:
        namesd = {}
        if os.path.exists(globs.sysprofilesdir):
            namesd["sys"] = [name[:-5] for name in os.listdir(globs.sysprofilesdir) if name.endswith(".conf")]
        if os.path.exists(globs.userprofilesdir):
            namesd["user"] = [name[:-5] for name in os.listdir(globs.userprofilesdir) if name.endswith(".conf")]
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
    return namesd

def get_profile_paths(name=None):
    """Get profile paths.
    """
    try:
        paths = []
        filename = "%s.conf" % name
        paths.append(os.path.join(globs.sysprofilesdir, "base.conf"))
        paths.append(os.path.join(globs.sysprofilesdir, "site.conf"))
        if name:
            paths.append(os.path.join(globs.sysprofilesdir, filename))
        paths.append(os.path.join(globs.userprofilesdir, "base.conf"))
        paths.append(os.path.join(globs.userprofilesdir, "site.conf"))
        if name:
            paths.append(os.path.join(globs.userprofilesdir, filename))
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
    return paths

def list_profiles():
    """Output list of profiles with sys and user prefixes.
    """
    namesd = get_profile_names()
    for ptype in ["sys", "user"]:
        names = sorted(namesd.get(ptype, []))
        if names:
            print("\n".join(["%s:%s" % (ptype, name) for name in names]))

def load_profiles(profname):
    """Load profile files and a Profile object and return.
    """
    try:
        prof = Profile()
        prof.load(get_profile_paths(profname))
        return prof
    except:
        raise

def show_profile(prof, fmt):
    if fmt == "json":
        print(json.dumps(prof.sections, indent=4, sort_keys=True))
    else:
        secnames = prof.sections.keys()
        if "default" in secnames:
            secnames.remove("default")
            secnames.insert(0, "default")

        for secname in secnames:
            if secname != "default":
                print()
            print("[%s]" % secname)
            setts = prof.sections[secname]
            # ordered, do not sort!
            for k, v in prof.sections[secname]:
                print("%s = %s" % (k, v))
