#! /usr/bin/env python3
#
# jobgen/generators/slurm.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

from os import environ

from jobgen import libconvert
from jobgen.generators.base import Generator, GeneratorException, get_nodegroups

class SlurmBaseGenerator(Generator):

    def __init__(self, reqinfo):
        Generator.__init__(self, reqinfo)
        self.prefix = "#SBATCH"
        self.toppost = True

    def _generate_jobfile(self, ulines):
        """Generate jobfile Base to Slurm.
        """
        reqinfo = self.reqinfo

        # set script to call login shell
        shell = reqinfo.get("request.shell")
        if shell:
            ulines.insert(0, "#! %s -l" % shell)

        # settings
        if "request.mail" in reqinfo:
            self.add("--mail-user=%s" % reqinfo.get("request.mail"))
        if "request.mailopts" in reqinfo:
            mailopts = reqinfo.get("request.mailopts").split(",")
            mopts = []
            if "beginning" in mailopts:
                mopts.append("BEGIN")
            if "end" in mailopts:
                mopts.append("END")
            if "aborted":
                mopts.append("FAIL")
            if "none":
                mopts = ["NONE"]
            if mopts:
                self.add("--mail-type=%s" % "".join(mopts))
        if "request.name" in reqinfo:
            self.add("--job-name=%s" % reqinfo.get("request.name"))

        self.add("--open-mode=append")

        if "request.outpath" in reqinfo:
            self.add("--output=%s" % reqinfo.get("request.outpath"))
        # slurm: out and err joined by default
        if not reqinfo.get("request.joinouterr"):
            if "request.errpath" in reqinfo:
                self.add("--error=%s" % reqinfo.get("request.errpath"))

        #if "request.project" in reqinfo:
            #self.add("-A %s" % reqinfo.get("request.project"))
        rerun = reqinfo.get("request.rerun")
        if rerun:
            self.add("--requeue")
        else:
            self.add("--no-requeue")
        #if "request.shell" in reqinfo:
            #self.add("-S %s" % reqinfo.get("request.shell"))

        # env vars
        l = []
        for k in sorted(reqinfo.keys()):
            if k.startswith("request.env."):
                v = reqinfo.sget(k)
                k = k[12:]
                if not v:
                    v = environ.get(k)
                if v != None:
                    l.append("%s=%s" % (k, v))
        if l:
            self.add("--export=%s" % ",".join(l))

        partition = reqinfo.get("qs.slurm.request.partition")
        if partition == None:
            raise GeneratorException("cannot find qs.slurm.request.partition setting")
        if partition:
            self.add("--partition=%s" % partition)
        if "request.wallclock" in reqinfo:
            self.add("--time=%s" % libconvert.convert_seconds2timefmt(reqinfo.get("request.wallclock"), "h:m:s"))

        if "request.jobarray" in reqinfo:
            self.add("--array=%s-%s:%s" % reqinfo.get("request.jobarray"))

    def get_nodegroups(self):
        nodegroups = get_nodegroups(self.reqinfo)
        if len(nodegroups) != 1:
            raise GeneratorException("generator only supports 1 chunk")
        return nodegroups

class SlurmGenerator(SlurmBaseGenerator):

    def __init__(self, reqinfo):
        SlurmBaseGenerator.__init__(self, reqinfo)

    def _generate_jobfile(self, ulines):
        """Generate jobfile for basic Slurm.
        """
        SlurmBaseGenerator._generate_jobfile(self, ulines)
        reqinfo = self.reqinfo

        nodegroups = self.get_nodegroups()
        nodegroup = nodegroups[0]

        nslots = reqinfo.get("request.chunk.0.%s.nslots" % nodegroup, 1)
        self.add("--ntasks=%s" % nslots)
        ncores = reqinfo.get("request.chunk.0.%s.ncores" % nodegroup, 1)
        self.add("--cpus-per-task=%s" % ncores)

        mem = reqinfo.get("request.chunk.0.%s.memory" % nodegroup)
        if mem:
            self.add("--mem-per-cpu=%s" % libconvert.convert_bytes2memoryfmt(int(mem/ncores), "M"))

class GPSCSlurmGenerator(SlurmGenerator):

    def __init__(self, reqinfo):
        SlurmBaseGenerator.__init__(self, reqinfo)

    def _generate_jobfile(self, ulines):
        """Generate for GPSC-specific Slurm.
        """
        SlurmGenerator._generate_jobfile(self, ulines)
        reqinfo = self.reqinfo

        nodegroups = self.get_nodegroups()
        nodegroup = nodegroups[0]

        l = []
        value = reqinfo.get("request.chunk.0.%s.image" % nodegroup)
        if value != None:
            l.append("image=%s" % value)
        l.append("ssh=true")
        l.append("nsswitch=true")

        self.add("""--comment="%s\"""" % ",".join(l))
