#! /usr/bin/env python3
#
# jobgen/generators/pbs.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

from os import environ

from jobgen import libconvert
from jobgen.generators.base import Generator, GeneratorException, get_nodegroups

class PBSBaseGenerator(Generator):

    def __init__(self, reqinfo):
        Generator.__init__(self, reqinfo)
        self.prefix = "#PBS"
        self.toppost = True

    def _generate_jobfile(self, ulines):
        """Generate jobfile Base to PBS and PBSPro.
        """
        reqinfo = self.reqinfo

        if "request.errpath" in reqinfo:
            self.add("-e %s" % reqinfo.get("request.errpath"))
        if reqinfo.get("request.joinouterr"):
            self.add("-j oe")
        if "request.mail" in reqinfo:
            self.add("-M %s" % reqinfo.get("request.mail"))
        if "request.mailopts" in reqinfo:
            mailopts = reqinfo.get("request.mailopts").split(",")
            mopts = []
            if "beginning" in mailopts:
                mopts.append("b")
            if "end" in mailopts:
                mopts.append("e")
            if "aborted":
                mopts.append("a")
            if "none":
                mopts = []
            if mopts:
                self.add("-m %s" % "".join(mopts))
        if "request.name" in reqinfo:
            self.add("-N %s" % reqinfo.get("request.name"))
        if "request.outpath" in reqinfo:
            self.add("-o %s" % reqinfo.get("request.outpath"))
        if "request.project" in reqinfo:
            self.add("-A %s" % reqinfo.get("request.project"))
        if "request.rerun" in reqinfo:
            self.add("-r %s" % (reqinfo.get("request.rerun") and "y" or "n"))
        if "request.shell" in reqinfo:
            self.add("-S %s" % reqinfo.get("request.shell"))

        # env vars
        for k in sorted(reqinfo.keys()):
            if k.startswith("request.env."):
                v = reqinfo.sget(k)
                k = k[12:]
                if not v:
                    v = environ.get(k)
                if v != None:
                    self.add("-v %s=%s" % (k, v))

        queue = reqinfo.get("qs.pbs.request.queue")
        if queue == None:
            raise GeneratorException("cannot find qs.pbs.request.queue setting")
        if queue:
            self.add("-q %s" % queue)
        if "request.wallclock" in reqinfo:
            self.add("-l walltime=%s" % libconvert.convert_seconds2timefmt(reqinfo.get("request.wallclock"), "h:m:s"))

    def get_nodegroups(self):
        nodegroups = get_nodegroups(self.reqinfo)
        if len(nodegroups) != 1:
            raise GeneratorException("generator only supports 1 chunk")
        return nodegroups

class PBSGenerator(PBSBaseGenerator):

    def __init__(self, reqinfo):
        PBSBaseGenerator.__init__(self, reqinfo)

    def _generate_jobfile(self, ulines):
        """Generate jobfile for PBS.
        """
        PBSBaseGenerator._generate_jobfile(self, ulines)
        reqinfo = self.reqinfo

        nodegroups = self.get_nodegroups()
        nodegroup = nodegroups[0]

        if "request.jobarray" in reqinfo:
            self.add("-t %s-%s:%s" % reqinfo.get("request.jobarray"))

        nslots = reqinfo.get("request.chunk.0.%s.nslots" % nodegroup, 1)
        ncores = reqinfo.get("request.chunk.0.%s.ncores" % nodegroup, 1)
        self.add("-l nodes=%s:ppn=%s" % (nslots, ncores))

        mem = reqinfo.get("request.chunk.0.%s.memory" % nodegroup)
        if mem:
            self.add("-l mem=%s" % int(nslots*mem))
