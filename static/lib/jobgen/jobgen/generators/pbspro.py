#! /usr/bin/env python3
#
# jobgen/generators/pbspro.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

from jobgen import libconvert
from jobgen.generators.base import Generator, GeneratorException, get_nodegroups
from jobgen.generators.pbs import PBSBaseGenerator

class PBSProGenerator(PBSBaseGenerator):

    def __init__(self, reqinfo):
        PBSBaseGenerator.__init__(self, reqinfo)

    def get_selects(self, chunkid, nodegroup):
        """Return list of "select" items. (Not slots.)
        """
        reqinfo = self.reqinfo
        selects = []

        # request.chunk.0.raw.*
        for k in reqinfo.keys():
            if k.startswith("request.chunk.%s.%s.raw." % (chunkid, nodegroup)):
                kk = k[20:]
                selects.append("%s=%s" % (kk, reqinfo.get(k)))

        # supported
        value = reqinfo.get("request.chunk.%s.%s.ncores" % (chunkid, nodegroup), 1)
        if value != None:
            selects.append("ncpus=%s" % value)
        value = reqinfo.get("request.chunk.%s.%s.memory" % (chunkid, nodegroup))
        if value != None:
            selects.append("mem=%s" % libconvert.convert_bytes2memoryfmt(value, "M"))
        return selects

    def _generate_jobfile(self, ulines):
        """Generate jobfile for PBSPro.
        """
        PBSBaseGenerator._generate_jobfile(self, ulines)
        reqinfo = self.reqinfo

        nodegroups = self.get_nodegroups()

        xselects = []
        for chunkid, nodegroup in enumerate(nodegroups):
            selects = [str(reqinfo.get("request.chunk.%s.%s.nslots", 1))]
            selects.extend(self.get_selects(chunkid, nodegroup))
            xselects.extend(":".join(selects))
        self.add("-l select=%s" % "+".join(xselects))

    def get_nodegroups(self):
        nodegroups = get_nodegroups(self.reqinfo)
        if len(nodegroups) < 1:
            raise GeneratorException("generator requires 1 or more chunks")
        return nodegroups

class PPPPBSProGenerator(PBSProGenerator):

    def get_selects(self, chunkid, nodegroup):
        """Return list of "select" items. (Not slots.)
        """
        reqinfo = self.reqinfo
        selects = []

        value = reqinfo.get("request.chunk.%s.%s.tmpfs" % (chunkid, nodegroup))
        if value != None:
            selects.append("res_tmpfs=%s" % libconvert.convert_bytes2memoryfmt(value, "M")[:-1])

        value = reqinfo.get("request.chunk.%s.%s.image" % (chunkid, nodegroup))
        if value != None:
            selects.append("res_image=%s" % value)
        return selects

    def _generate_jobfile(self, ulines):
        PBSBaseGenerator._generate_jobfile(self, ulines)
        reqinfo = self.reqinfo

        if "request.jobarray" in reqinfo:
            self.add("-J %s-%s:%s" % reqinfo.get("request.jobarray"))

        nodegroups = self.get_nodegroups()

        xselects = []
        for chunkid, nodegroup in enumerate(nodegroups):
            selects = [str(reqinfo.get("request.chunk.%s.%s.nslots", 1))]
            selects.extend(PBSProGenerator.get_selects(self, chunkid, nodegroup))
            selects.extend(self.get_selects(chunkid, nodegroup))
            xselects.append(":".join(selects))
        self.add("-l select=%s" % "+".join(xselects))

        if "qs.pbspro.request.place" in reqinfo:
            self.add("-l place=%s" % (reqinfo.get("qs.pbspro.request.place"),))
