#! /usr/bin/env python3
#
# jobgen/generators/__init__.py

__all__ = ["get_generator", "get_generator_names", "get_nodegroups"]

import traceback

from jobgen import globs
from jobgen.profile import load_profiles

def get_generator(reqinfo):
    try:
        qsg = None
        genimport = reqinfo.get("generator.import")
        modname, classname = genimport.rsplit(".", 1)
        mod = __import__("jobgen.generators.%s" % modname, globals(), locals(), [classname], 0)
        qsg = mod.__dict__[classname](reqinfo)
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
    return qsg

def get_generator_names():
    try:
        names = []
        names.extend(globs.conf.options("generators"))
    except:
        globs.logger.debug("traceback (%s)" % traceback.format_exc())
    return names

def list_generators(profname):
    names = []
    prof = load_profiles(profname)
    for k, v in sorted(prof.items("default")):
        if k.startswith("generator."):
            t = k.split(".")
            names.append(t[1])
    if names:
        print("\n".join(sorted(names)))
