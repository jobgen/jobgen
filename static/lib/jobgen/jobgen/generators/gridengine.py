#! /usr/bin/env python3
#
# jobgen/generators/gridengine.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

from os import environ

from jobgen import libconvert
from jobgen.generators.base import Generator, GeneratorException, get_nodegroups

class GridengineBaseGenerator(Generator):

    def __init__(self, reqinfo):
        Generator.__init__(self, reqinfo)
        self.prefix = "#$"
        self.toppost = False

    def _generate_jobfile(self, ulines):
        """Generate jobfile for Gridengine.
        """
        reqinfo = self.reqinfo

        nodegroups = self.get_nodegroups()
        nodegroup = nodegroups[0]

        if "request.errpath" in reqinfo:
            self.add("-e %s" % reqinfo.get("request.errpath"))
        if "request.joinouterr" in reqinfo:
            self.add("-j %s" % (reqinfo.get("request.joinouterr") and "y" or "n"))
        if "request.mail" in reqinfo:
            self.add("-M %s" % reqinfo.get("request.mail"))
        if "request.mailopts" in reqinfo:
            mailopts = reqinfo.get("request.mailopts").split(",")
            mopts = []
            if "beginning" in mailopts:
                mopts.append("b")
            if "end" in mailopts:
                mopts.append("e")
            if "aborted":
                mopts.append("a")
            if "suspended":
                mopts.append("s")
            if "none":
                mopts = []
            if mopts:
                self.add("-m %s" % "".join(mopts))
        if "request.name" in reqinfo:
            self.add("-N %s" % reqinfo.get("request.name"))
        if "request.outpath" in reqinfo:
            self.add("-o %s" % reqinfo.get("request.outpath"))
        if "request.project" in reqinfo:
            self.add("-P %s" % reqinfo.get("request.project"))
        if "request.rerun" in reqinfo:
            self.add("-r %s" % (reqinfo.get("request.rerun") and "y" or "n"))
        if "request.shell" in reqinfo:
            self.add("-S %s" % reqinfo.get("request.shell"))

        # env vars
        for k in sorted(reqinfo.keys()):
            if k.startswith("request.env."):
                v = reqinfo.sget(k)
                k = k[12:]
                if not v:
                    v = environ.get(k)
                if v != None:
                    self.add("-v %s=%s" % (k, v))

        if "request.jobarray" in reqinfo:
            self.add("-t %s-%s:%s" % reqinfo.get("request.jobarray"))

        nslots = reqinfo.get("request.chunk.0.%s.nslots" % nodegroup, 1)
        pe = reqinfo.get("qs.gridengine.request.pe")
        if pe == None:
            raise GeneratorException("cannot find qs.gridengine.request.pe setting")
        if pe:
            self.add("-pe %s %s" % (pe, nslots))

        queue = reqinfo.get("qs.gridengine.request.queue")
        if queue:
            self.add("-q %s" % queue)
        if "request.wallclock" in reqinfo:
            self.add("-l h_rt=%s" % libconvert.convert_seconds2timefmt(reqinfo.get("request.wallclock"), "h:m:s"))

        # request.chunk.0.<nodegroup>.raw.*
        rawkeypref = "request.chunk.0.%s.raw." % nodegroup
        for k in reqinfo.keys():
            if k.startswith(rawkeypref):
                kk = k[20:]
                self.add("-l %s=%s" % (kk, reqinfo.get(k)))

    def get_nodegroups(self):
        nodegroups = get_nodegroups(self.reqinfo)
        if len(nodegroups) != 1:
            raise GeneratorException("generator only supports 1 chunk")
        return nodegroups

class GridengineGenerator(GridengineBaseGenerator):

    def __init__(self, reqinfo):
        GridengineBaseGenerator.__init__(self, reqinfo)

class GPSCGridengineGenerator(GridengineBaseGenerator):

    def __init__(self, reqinfo):
        GridengineBaseGenerator.__init__(self, reqinfo)

    def _generate_jobfile(self, ulines):
        GridengineBaseGenerator._generate_jobfile(self, ulines)
        reqinfo = self.reqinfo

        nodegroups = self.get_nodegroups()
        nodegroup = nodegroups[0]

        value = reqinfo.get("request.chunk.0.%s.gputype" % nodegroup)
        if value != None:
            self.add("-l res_gputype=%s" % value)

        value = reqinfo.get("request.chunk.0.%s.image" % nodegroup)
        if value != None:
            self.add("-l res_image=%s" % value)

        value = reqinfo.get("request.chunk.0.%s.memory" % nodegroup)
        if value != None:
            self.add("-l res_mem=%s" % libconvert.convert_bytes2memoryfmt(value, "M")[:-1])

        value = reqinfo.get("request.chunk.0.%s.ncores" % nodegroup)
        if value != None:
            self.add("-l res_cpus=%s" % value)

        value = reqinfo.get("request.chunk.0.%s.ngpus" % nodegroup)
        if value != None:
            self.add("-l res_gpus=%s" % value)

        value = reqinfo.get("request.chunk.0.%s.tmpfs" % nodegroup)
        if value != None:
            self.add("-l res_tmpfs=%s" % libconvert.convert_bytes2memoryfmt(value, "M")[:-1])
