#! /usr/bin/env python3
#
# jobgen/generators/base.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import time

class GeneratorException(Exception):
    pass

class Generator:

    def __init__(self, reqinfo):
        self.reqinfo = reqinfo
        self.l = []
        self.prefix = None
        self.toppost = None

    def add(self, value):
        self.l.append("%s %s" % (self.prefix, value))

    def cadd(self, cond, value):
        if cond:
            self.l.append("%s %s" % (self.prefix, value))

    def _generate_jobfile(self, ulines):
        """Override for each Generator.
        """
        return

    def generate_jobfile(self, ulines):
        """Generate jobfile for specific queueing system.

        Calls self._generate_jobfile() to perform the actual
        generation.

        ulines - unused lines from the original job file
        """
        self._generate_jobfile(ulines)
        reqinfo = self.reqinfo

        lines = []
        lines.extend([
            "",
            "# ---- jobgen -- start",
            "# ---- jobgen -- timestamp (%s)" % time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()),
            "# ---- jobgen -- generator (%s)" % self.__class__,
            "# ---- jobgen -- syshooks (%s) (%s) (%s)" \
                % (reqinfo.sget("request.syshooks.names"), reqinfo.sget("request.syshooks.allow"),
                    reqinfo.sget("request.syshooks.deny")),
            "# ---- jobgen -- hooks (%s) (%s) (%s)" \
                % (reqinfo.sget("request.hooks.names"), reqinfo.sget("request.hooks.allow"),
                    reqinfo.sget("request.hooks.deny")),
            "#",
        ])
        lines.extend(self.l)
        lines.extend([
            "#",
            "# ---- jobgen -- end",
            "",
        ])

        if self.toppost:
            if ulines and ulines[0].startswith("#!"):
                lines = [ulines[0]]+lines+ulines[1:]
            else:
                lines = lines+ulines
        else:
            lines = ulines+lines

        return lines

    def get_nodegroups(self):
        """Return list of nodegroups found, one per chunk.
        """
        return get_nodegroups(self.reqinfo)

def get_nodegroups(reqinfo):
    """Get nodegroups.
    """
    nodegroups = []
    nchunks = int(reqinfo.get("request.nchunks", 1))
    for chunkid in range(nchunks):
        prefkey = "request.chunk.%s" % chunkid
        keys = reqinfo.match(prefkey)
        x = set()
        for key in keys:
            comps = key.split(".")
            x.add(comps[3])
        if len(x) != 1:
            raise GeneratorException("only one nodegroup per chunk allowed (%s)" % (x,))
        nodegroups.append(x.pop())
    return nodegroups
