#! /usr/bin/env python3
#
# jobgen/lib.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import grp
import os
import pwd
import traceback

def get_groupnames():
    gnames = []
    for gid in os.getgroups():
        gr = grp.getgrgid(gid)
        gnames.append(gr.gr_name)
    return gnames

def whoami_all():
    pw = pwd.getpwuid(os.getuid())
    gr = grp.getgrgid(pw.pw_gid)
    return pw.pw_name, gr.gr_name, get_groupnames()
