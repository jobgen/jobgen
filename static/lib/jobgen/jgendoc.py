#! /usr/bin/env python3
#
# jgendoc.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

# disable writing bytecode (uses encoding hack)
import sys as _sys
_sys.dont_write_bytecode = True

import json
import os
import os.path
import pprint
import subprocess
import sys
from sys import stderr
import traceback

import hte

from jobgen.constants import VERSION
from jobgen.profile import load_profiles
from jobgen.reqinfo import RequestInfo

PROPS_BLACKLIST = set(["description", "keytype", "type", "value"])

def get(d, secname, k):
    dd = d.get("default", {})
    v = dd.get(k, {})
    if secname:
        dd = d.get(secname, {})
        v.update(dd.get(k, {}))
    return v

def getkeys(d, secname):
    keys = []
    dd = d.get("default", {})
    keys.extend(dd.keys())
    if secname:
        dd = d.get(secname, {})
        keys.extend(dd.keys())
    return keys

def get_info(profname=None, qname=None, itype=None):
    pargs = ["jobgen"]
    if itype == "meta":
        #pargs.append("--show-profile-flat=json")
        pargs.append("--show-meta=json")
    elif itype == "profile":
        pargs.append("--show-profile=json")
    elif itype == "request":
        pargs.append("--show-request=json")
    elif itype == "request-flat":
        pargs.append("--show-request-flat=json")
    if profname:
        pargs.extend(["-p", profname])
    if qname:
        pargs.extend(["-r", "queue=%s" % qname])
    p = subprocess.Popen(pargs, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    sout, serr = p.communicate()
    if p.returncode != 0:
        stderr.write("error: failed to run (%s) (%s)\n" % (p.returncode, serr.strip()))
        sys.exit(1)
    d = json.loads(sout)
    return d

def stripquotes(s):
    return str(s).strip("\"'")

def print_usage():
    print("""\
usage: jgendoc [<options>] -p <profile>
       jgendoc -h|--help

Load profile and generate a document for configuration.

Where:
-p <profile>        Profile to load.
--version           Print version.

Options:
--fmt <fmt>         Output format. Default is html.
--namespaces <name>[,...]
                    Provide information for namespaces. Default is request.
--skip-title        Do not output a title.
--toc-code <html>   Insert table of contents snippet at the very top
                    of the page.""")

if __name__ == "__main__":
    try:
        debug = False
        fmt = "html"
        namespaces = ["request"]
        profname = None
        skiptitle = False
        toccode = None

        args = sys.argv[1:]
        while args:
            arg = args.pop(0)
            if arg == "--toc-code":
                toccode = args.pop(0)
            elif arg == "--fmt":
                fmt = args.pop(0)
            elif arg == "--namespaces":
                namespaces = args.pop(0).split(",")
            elif arg == "-p":
                profname = args.pop(0)
            elif arg == "--skip-title":
                skiptitle = True
            elif arg in ["-h", "--help"]:
                print_usage()
                sys.exit(0)
            elif arg == "--debug":
                debug = True
            elif arg == "--version":
                print(VERSION)
                sys.exit(0)
            else:
                raise Exception()

        if fmt not in ["html", "text"]:
            raise Exception()
        if fmt == "text":
            try:
                import html2text
            except:
                stderr.write("error: missing html2text package to generate text output")
                sys.exit(1)
    except SystemExit:
        raise
    except:
        if debug:
            traceback.print_exc()
        stderr.write("error: bad/missing arguments\n")
        sys.exit(1)

    try:
        tb = hte.XHtml5TreeBuilder()

        # hack to adjust head level
        if skiptitle:
            tbh1 = None
            tbh2 = tb.h1
            tbh3 = tb.h2
            tbh4 = tb.h3
        else:
            tbh1 = tb.h1
            tbh2 = tb.h2
            tbh3 = tb.h3
            tbh4 = tb.h4

        body = tb.body()

        if toccode:
            body.add(hte.Raw(toccode))

        if not skiptitle:
            body.add(tbh1("Jobgen: %s" % profname))

        # site section
        flatd = get_info(profname, itype="request-flat")
        url = stripquotes(flatd.get("info.site.url"))

        body.add(tbh2("Site"))
        table = body.add(tb.table())
        table.add(
            tb.tr(tb.th("Name"), tb.td(stripquotes(flatd.get("info.site.name")))),
            tb.tr(tb.th("Description"), tb.td(stripquotes(flatd.get("info.site.description")))),
            tb.tr(tb.th("Location"), tb.td(stripquotes(flatd.get("info.site.location")))),
            tb.tr(tb.th("Contact"), tb.td(stripquotes(flatd.get("info.site.contact")))),
            tb.tr(tb.th("URL"), tb.td(tb.a(url, _href=url))),
            tb.tr(tb.th("Alert"), tb.td(stripquotes(flatd.get("info.site.alert")))),
        )

        # system section
        flatd = get_info(profname, itype="request-flat")
        url = stripquotes(flatd.get("info.system.url"))

        body.add(tbh2("System"))
        table = body.add(tb.table())
        table.add(
            tb.tr(tb.th("Name"), tb.td(stripquotes(flatd.get("info.system.name")))),
            tb.tr(tb.th("Description"), tb.td(stripquotes(flatd.get("info.system.description")))),
            tb.tr(tb.th("Location"), tb.td(stripquotes(flatd.get("info.system.location")))),
            tb.tr(tb.th("Contact"), tb.td(stripquotes(flatd.get("info.system.contact")))),
            tb.tr(tb.th("URL"), tb.td(tb.a(url, _href=url))),
            tb.tr(tb.th("Alert"), tb.td(stripquotes(flatd.get("info.system.alert")))),
        )

        # queuing system section
        flatd = get_info(profname, itype="request-flat")

        body.add(tbh2("Queueing System"))
        table = body.add(tb.table())
        table.add(
            tb.tr(tb.th("Name"), tb.td(stripquotes(flatd.get("info.qs.name")))),
            tb.tr(tb.th("Description"), tb.td(stripquotes(flatd.get("info.qs.description")))),
            tb.tr(tb.th("Alert"), tb.td(stripquotes(flatd.get("info.qs.alert")))),
        )

        #
        # Queues section
        #
        profd = get_info(profname, itype="profile")
        secnames = sorted(profd.keys())
        qnames = sorted([secname[6:] for secname in secnames if secname.startswith("queue.")])

        body.add(tbh2("Queues"))
        for qname in qnames:
            body.add(tbh3(qname))

            body.add(tbh4("Information"))
            table = body.add(tb.table())
            table.add(
                tb.tr(tb.th("Name"), tb.td(stripquotes(flatd.get("info.queue.name")))),
                tb.tr(tb.th("Description"), tb.td(stripquotes(flatd.get("info.queue.description")))),
                tb.tr(tb.th("Alert"), tb.td(stripquotes(flatd.get("info.queue.alert")))),
            )

            body.add(tbh4("Settings"))
            table = body.add(tb.table())
            #table.add(tb.tr(tb.th("Key"), tb.th("Description"), tb.th("Type"), tb.th("Value"), tb.th("Properties")))
            #table.add(tb.tr(tb.th("Key"), tb.th("Description"), tb.th("[Type] Value"), tb.th("Properties")))
            #table.add(tb.tr(tb.th("Key"), tb.th("Description"), tb.th("Type"), tb.th("Properties")))
            table.add(tb.tr(tb.th("Key"), tb.th("Description"), tb.th("Properties")))

            reqd = get_info(profname, qname, itype="meta")
            reqflatd = get_info(profname, qname, itype="request-flat")
            allkeys = set(list(reqd.keys())+list(reqflatd.keys()))
            for k in sorted(allkeys):
                nsname, _ = k.split(".", 1)
                if nsname in namespaces:
                    if k.startswith("request.chunk."):
                        # special case!
                        t = k.split(".")
                        kk = "request.chunk.*.%s" % (".".join(t[3:]))
                        dd = reqd.get(kk, {})
                        dd.update(reqd.get(k, {}))
                    else:
                        dd = reqd.get(k, {})
                    vtype = dd.pop("type", "string")
                    value = reqflatd.get(k, "")
                    if vtype == "namelist":
                        value = value.replace(",", ", ")
                    description = stripquotes(dd.get("description", ""))
                    l = ["%s=%s" % t for t in sorted(dd.items()) if t[0] not in PROPS_BLACKLIST]
                    ul = tb.ul([tb.li(x) for x in l])
                    ul.add(tb.li("type=", tb.em(vtype)))
                    if k in reqflatd:
                        #ul.add(tb.li(tb.strong("value=", value)))
                        ul.add(tb.li("value=", tb.strong(value)))
                    #table.add(tb.tr(tb.td(tb.code(k)), tb.td(description), tb.td(vtype), tb.td(value), tb.td(ul)))
                    #table.add(tb.tr(tb.td(tb.code(k)), tb.td(description), tb.td(vtype, tb.br(), value), tb.td(ul)))
                    #table.add(tb.tr(tb.td(tb.code(k)), tb.td(description), tb.td(tb.ul(tb.li(vtype), tb.li(value))), tb.td(ul)))
                    #table.add(tb.tr(tb.td(tb.code(k)), tb.td(description), tb.td("[", vtype, "]", " ", value), tb.td(ul)))
                    #table.add(tb.tr(tb.td(tb.code(k)), tb.td(description), tb.td("[", vtype, "]", " ", tb.code(value)), tb.td(ul)))
                    #table.add(tb.tr(tb.td(tb.code(k)), tb.td(description), tb.td(vtype), tb.td(ul)))
                    table.add(tb.tr(tb.td(tb.code(k)), tb.td(description), tb.td(ul)))

        s = body.render()
        if fmt == "text":
            s = html2text.html2text(s)
        print(s)
    except SystemExit:
        raise
    except:
        if debug:
            traceback.print_exc()
        stderr.write("error: failed to run\n")
        sys.exit(1)
