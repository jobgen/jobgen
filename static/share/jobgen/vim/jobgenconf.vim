" Vim syntax file
" Language:     JobgenConf
" Maintainer:   John Marshall
"
" Last Change:  2019-07-11

if version <600
    syntax clear
elsif exists("b:current_syntax")
    finish
endif

syn clear
hi clear

syn match	jobgenconfSetting		"^[^:=]\{-1,}="
syn match	jobgenconfSettingOnly		"^[^:=]\{-1,}$"
syn match	jobgenconfMeta			"^meta\.[^:=.]\{-1,}\."
syn match	jobgenconfSection skipwhite	"^\[.\+\]$"
syn match       jobgenconfComment skipwhite	"^[#;].*"

if version >= 508 || !exists("did_acedb_syn_inits")
    if version < 508
        let did_acedb_syn_inits = 1
        command -nargs=+ HiLink hi link <args>
    else
        command -nargs=+ HiLink hi def link <args>
    endif

    HiLink      jobgenconfComment		Comment

    HiLink      jobgenconfSection		Statement
    HiLink      jobgenconfMeta			Define
    HiLink      jobgenconfSetting		Identifier
    HiLink      jobgenconfSettingOnly		Identifier
  delcommand HiLink
endif

